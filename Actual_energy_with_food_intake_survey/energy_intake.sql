/* Query 1, clean up invalid users */
CREATE VIEW nutrition_valid AS
SELECT *
FROM valid_user JOIN diatery_data ON (ID1=record_id)

/* Query 2, Joining two tables: nutrition and daily_survey */
SELECT *
FROM diatery_data D JOIN daily_survey S ON (D.date=S.DATE) JOIN valid_user V ON (D.ID1=V.record_id)
WHERE V.login = S.username
ORDER BY id1 ASC, D.date ASC

/* Query 3, actual energy on different value users */
SELECT id1, T.date, participant_day_id, total_energy, q1_value
FROM (
SELECT id1, login, date, participant_day_id, SUM(energy__with_dietary_fibre__kj_) AS total_energy
FROM diatery_data JOIN valid_user ON (id1=record_id)
GROUP BY id1, login, date, participant_day_id
--HAVING count(id1)=3
ORDER BY id1 ASC, date ASC
) T
JOIN daily_survey S ON (T.login=S.username)
WHERE T.date = S.date


/* Query 4: actual energy on user with variations */
SELECT id1, T.date, participant_day_id, total_energy, q1_value
FROM (
SELECT D.id1, U.login, date, participant_day_id, SUM(energy__with_dietary_fibre__kj_) AS total_energy
FROM diatery_data D JOIN user_id_username_daily_survey_q1_variation U ON (D.id1=U.id1)
GROUP BY D.id1, U.login, date, participant_day_id
ORDER BY D.id1 ASC, date ASC
) T
JOIN daily_survey S ON (T.login=S.username)
WHERE T.date = S.date
