/* Query 1 count number of entries in nutrition */
SELECT COUNT(*)
FROM diatery_data
;

/* Query 2 count number of entries who eat more but have usual physical activity */
SELECT COUNT(*)
FROM daily_survey S --INNER JOIN valid_user V ON (S.username = V.login)
WHERE q5_value = 1 AND q1_value = 0;

/* Query 3 Match daily_survey data with valid_user table */
SELECT *
FROM daily_survey S INNER JOIN valid_user V ON (S.username= V.login)
WHERE S.date = diatery_data.date
;

/* Query 4 count number of entries who sleep less and have less food */
SELECT count(*)
FROM daily_survey S INNER JOIN valid_user V ON (S.username= V.login)
WHERE q7_value=2 AND q1_value=2;
