CREATE VIEW user_survey_q1_variations AS

SELECT id1, q1_value, count(q1_value) AS count_q1_value
FROM (
SELECT id1, login, date, participant_day_id, SUM(energy__with_dietary_fibre__kj_) AS total_energy
FROM diatery_data JOIN valid_user ON (id1=record_id)
GROUP BY id1, login, date, participant_day_id
--HAVING count(id1)=3
ORDER BY id1 ASC, date ASC
) T
JOIN daily_survey S ON (T.login=S.username)
WHERE T.date = S.date
GROUP BY id1, q1_value


CREATE VIEW user_id_daily_survey_q1_variation AS
SELECT id1, COUNT(id1)--, q1_value--, count_q1_value
FROM user_survey_q1_variations
GROUP BY id1--, q1_value--, count_q1_value
HAVING count(id1)>1


CREATE VIEW user_id_username_daily_survey_q1_variation AS
SELECT id1, login
FROM valid_user V JOIN user_id_daily_survey_q1_variation U ON (V.record_id = U.id1)

SELECT id1, login, date, q1_value, q5_value, q6_value, q7_value
FROM user_id_username_daily_survey_q1_variation U JOIN daily_survey D ON (U.login = d.username)
ORDER BY id1, date
