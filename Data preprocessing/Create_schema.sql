﻿/* 2017_11_v1 data */
CREATE TABLE diatery_data (
	id VARCHAR(30),
	date DATE,
	day CHAR(3),
	ID1 int,
	Participant_day_ID varchar(10),
	check_2 VARCHAR(500),
	foodtype VARCHAR(100),
	AUSNUT_food_ID1 VARCHAR(100),
	foodName VARCHAR(500),
	Food_ID VARCHAR(10),
	serving_size FLOAT,
	total FLOAT,
	Energy__with_dietary_fibre__kJ_ VARCHAR(20),
	Protein__g_ VARCHAR(20),
	Total_fat__g_ VARCHAR(20),
	Carbohydrates VARCHAR(20),
	Total_sugars__g_ VARCHAR(20),
	Added_sugars__g_ VARCHAR(20),
	Dietary_fibre__g_ VARCHAR(20),
	Vitamin_A_retinol_equivalents___ VARCHAR(20),
	Thiamin__B1___mg_ VARCHAR(20),
	Riboflavin__B2___mg_ VARCHAR(20),
	Niacin__B3___mg_ VARCHAR(20),
	Total_Folates____g_ VARCHAR(20),
	Vitamin_B6__mg_ VARCHAR(20),
	Vitamin_B12____g_ VARCHAR(20),
	Vitamin_C__mg_ VARCHAR(20),
	Vitamin_E__mg_ VARCHAR(20),
	Calcium__Ca___mg_ VARCHAR(20),
	Iodine__I____g_ VARCHAR(20),
	Iron__Fe___mg_ VARCHAR(20),
	Magnesium__Mg___mg_ VARCHAR(20),
	Phosphorus__P___mg_ VARCHAR(20),
	Potassium__K___mg_ VARCHAR(20),
	Selenium__Se____g_ VARCHAR(20),
	Sodium__Na___mg_ VARCHAR(20),
	Zinc__Zn___mg_ VARCHAR(20),
	Saturated_fat__g_ VARCHAR(20),
	Monounsaturated_fat__g_ VARCHAR(20),
	Polyunsaturated_fat__g_ VARCHAR(20),
	Trans FLOAT
);

--DROP TABLE diatery_data;
--ALTER TABLE diatery_data ALTER COLUMN foodname TYPE VARCHAR(500);

/* Login completers gender.csv */
CREATE TABLE valid_user(
	record_id INT PRIMARY KEY,
	name VARCHAR(100),
	login VARCHAR(100) UNIQUE,
	gender CHAR(1)
);
--ALTER TABLE valid_user
--ADD CONSTRAINT unique_login UNIQUE (login);

--DROP TABLE valid_user;

/* dailyquestionaires */
CREATE TABLE daily_survey(
	id VARCHAR(30),
	date DATE,
	q1_value INT,
	q2_value INT,
	q3_check_1 VARCHAR(5) CHECK(q3_check_1 IN ('FALSE','TRUE')),
	q3_check_2 VARCHAR(5) CHECK(q3_check_2 IN ('FALSE','TRUE')),
	q3_check_3 VARCHAR(5) CHECK(q3_check_3 IN ('FALSE','TRUE')),
	q3_check_4 VARCHAR(5) CHECK(q3_check_4 IN ('FALSE','TRUE')),
	q3_check_5 VARCHAR(5) CHECK(q3_check_5 IN ('FALSE','TRUE')),
	q3_check_6 VARCHAR(5) CHECK(q3_check_6 IN ('FALSE','TRUE')),
	q3_check_6_answer VARCHAR(200),
	q4_value INT,
	q5_value INT,
	q6_value INT,
	q7_value INT,
	recordtime VARCHAR(20),
	username VARCHAR(100)
)
;



--After cleaning the table, the FOREIGN KEY constraint should hold
ALTER TABLE diatery_data
ADD CONSTRAINT id_fk FOREIGN KEY (ID1) REFERENCES valid_user (record_id);

ALTER TABLE daily_survey
ADD CONSTRAINT username_fk FOREIGN KEY (username) REFERENCES valid_user (login);
--DROP TABLE daily_survey;

--ALTER TABLE daily_survey ALTER COLUMN q3_check_6_answer TYPE VARCHAR(200);
--DROP TABLE IF EXISTS diatery_data;

/* Set Date */
--postgres=#
--set datestyle to DMY;
--SET
--postgres=# SELECT '19/08/2014'::date;
--    date
