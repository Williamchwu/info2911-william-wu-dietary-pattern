﻿--Clean data query
--Find id exist in diatery_data but not exist in valid_user
SELECT *
FROM diatery_data
WHERE id1 NOT IN (SELECT record_id FROM valid_user);

DELETE
FROM diatery_data
WHERE id1 NOT IN (SELECT record_id FROM valid_user);

--Find username exist in daily_survey but not exist in valid_user
SELECT *
FROM daily_survey
WHERE LOWER(username) NOT IN (SELECT LOWER(login) FROM valid_user);

DELETE
FROM daily_survey
WHERE LOWER(username) NOT IN (SELECT LOWER(login) FROM valid_user);

--Convert all username to lower case
UPDATE daily_survey SET username=lower(username)
UPDATE valid_user SET login=lower(login)
