﻿COPY diatery_data FROM '/Users/william/diatery_pattern_data/2017_11_v1_data_trim.csv' with delimiter ',' CSV HEADER;

COPY valid_user FROM '/Users/william/diatery_pattern_data/Login_details_trim.csv' with delimiter ',' CSV HEADER;

COPY daily_survey FROM '/Users/william/diatery_pattern_data/daily/dailyquestionnaires_20170911.csv' with delimiter ',' CSV HEADER;